﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    [Tooltip("velocidad de la bala")]
    public float bulletSpeed;


    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, bulletSpeed);
       
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}
