﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShot : MonoBehaviour
{
    [Tooltip("Referencia al prefab de la bala")]
    public GameObject bulletPrefab;

    public float timeLastShot;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            timeLastShot = Time.time;
        } 
    }


}
